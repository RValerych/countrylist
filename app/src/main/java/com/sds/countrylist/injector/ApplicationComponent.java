package com.sds.countrylist.injector;

import com.sds.countrylist.details.ui.DetailsActivity;
import com.sds.countrylist.home.ui.activity.HomeActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(HomeActivity homeActivity);

    void inject(DetailsActivity detailsActivity);
}

