package com.sds.countrylist;

import android.app.Application;

import com.sds.countrylist.injector.ApplicationComponent;
import com.sds.countrylist.injector.ApplicationModule;
import com.sds.countrylist.injector.DaggerApplicationComponent;

public class CountryListApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}