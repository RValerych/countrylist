package com.sds.countrylist.api;

import com.sds.countrylist.home.model.Country;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RestApiClient {

    @GET("rest/v2/all")
    Observable<List<Country>> getAllCountries();
}
