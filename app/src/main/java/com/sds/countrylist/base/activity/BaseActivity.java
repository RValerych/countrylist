package com.sds.countrylist.base.activity;
import android.support.v7.app.AppCompatActivity;

import com.sds.countrylist.base.presenter.BasePresenter;

import butterknife.ButterKnife;

/**
 * Base activity class with view binding and base methods
 */
public abstract class BaseActivity extends AppCompatActivity {

    /**
     * Binds all the views and actions. Injects all dependencies
     *
     * @param layoutResID activity layout file
     */
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        inject();
    }

    /**
     * Notifies presenter about view destroy
     */
    @Override
    protected void onDestroy() {
        getPresenter().onViewDestroyed();
        super.onDestroy();
    }

    /**
     * Injects all dependencies
     */
    protected abstract void inject();

    /**
     * Getter
     *
     * @return presenter for current activity
     */
    protected abstract BasePresenter getPresenter();
}