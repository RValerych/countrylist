package com.sds.countrylist.base.presenter;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Base presenter for most important methods.
 */
public abstract class BasePresenter {

    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void onViewDestroyed() {
        compositeDisposable.dispose();
    }
}