package com.sds.countrylist.home.model;

import java.io.Serializable;
import java.util.List;

public class Country implements Serializable {

    private String name;
    private String flag;
    private Integer population;
    private String alpha2Code;
    private String capital;
    private String region;
    private String subregion;
    private List<Double> latlng;
    private String demonym;
    private Double area;
    private List<String> timezones;
    private String nativeName;

    public String getName() {
        return name;
    }

    public String getFlag() {
        return flag;
    }

    public Integer getPopulation() {
        return population;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public List<Double> getLatlng() {
        return latlng;
    }

    public String getDemonym() {
        return demonym;
    }

    public Double getArea() {
        return area;
    }

    public List<String> getTimezones() {
        return timezones;
    }

    public String getNativeName() {
        return nativeName;
    }

    public boolean hasEnglishName() {
        return name != null && name.equals(nativeName);
    }
}
