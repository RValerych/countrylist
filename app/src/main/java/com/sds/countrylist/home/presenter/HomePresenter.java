package com.sds.countrylist.home.presenter;

import com.sds.countrylist.api.RestApiClient;
import com.sds.countrylist.base.presenter.BasePresenter;
import com.sds.countrylist.details.ui.DetailsActivity;
import com.sds.countrylist.home.model.Country;
import com.sds.countrylist.home.ui.activity.HomeView;
import com.sds.countrylist.home.ui.adapter.OnElementClickListener;
import com.sds.countrylist.utils.logger.Logger;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomePresenter extends BasePresenter {

    private final RestApiClient restApiClient;
    private HomeView homeView;

    @Inject
    public HomePresenter(RestApiClient restApiClient) {
        this.restApiClient = restApiClient;
    }

    public void setView(HomeView homeView) {
        this.homeView = homeView;
        loadCountries();
    }

    private void loadCountries() {
        restApiClient.getAllCountries().doOnSubscribe(compositeDisposable::add)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(homeView::setupCountries, error -> {
                    Logger.error(error);
                    homeView.showConnectionErrorMessage((dialogInterface, i) -> {
                        loadCountries();
                    });
                });
    }

    public OnElementClickListener getOnElementClickListener() {
        return country -> homeView.startActivity(DetailsActivity.getIntent(homeView.getApplicationContext(), country));
    }
}
