package com.sds.countrylist.home.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sds.countrylist.R;
import com.sds.countrylist.home.model.Country;
import com.sds.countrylist.utils.svg.SVGUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.ViewHolder> {

    private final List<Country> countries;
    private final Context context;
    private final OnElementClickListener onElementClickListener;

    public CountryListAdapter(Context context, List<Country> countries, OnElementClickListener onElementClickListener) {
        this.countries = countries;
        this.context = context;
        this.onElementClickListener = onElementClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.country_element, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.countryName.setText(countries.get(position).hasEnglishName() ? countries.get(position).getName() : countries.get(position).getName() + " (" + countries.get(position).getNativeName() + ")");
        holder.countryPopulation.setText(context.getString(R.string.population, countries.get(position).getPopulation()).replace(',', '.'));
        SVGUtils.insertSVGImage(context, countries.get(position).getFlag(), holder.countryFlag);
        holder.itemView.setOnClickListener(v -> onElementClickListener.onClick(countries.get(holder.getAdapterPosition())));
    }

    @Override
    public int getItemCount() {
        return countries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.country_flag)
        ImageView countryFlag;
        @BindView(R.id.country_name)
        TextView countryName;
        @BindView(R.id.country_population)
        TextView countryPopulation;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
