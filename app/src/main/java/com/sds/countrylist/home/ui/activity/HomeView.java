package com.sds.countrylist.home.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.sds.countrylist.home.model.Country;

import java.util.List;

public interface HomeView {

    Context getApplicationContext();

    void startActivity(Intent intent);

    void setupCountries(List<Country> countries);

    void showConnectionErrorMessage(DialogInterface.OnClickListener clickListenr);
}
