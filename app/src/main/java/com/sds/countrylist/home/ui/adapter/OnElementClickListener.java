package com.sds.countrylist.home.ui.adapter;

import com.sds.countrylist.home.model.Country;

public interface OnElementClickListener {

    void onClick(Country country);
}
