package com.sds.countrylist.home.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.sds.countrylist.CountryListApplication;
import com.sds.countrylist.R;
import com.sds.countrylist.base.activity.BaseActivity;
import com.sds.countrylist.base.presenter.BasePresenter;
import com.sds.countrylist.home.model.Country;
import com.sds.countrylist.home.presenter.HomePresenter;
import com.sds.countrylist.home.ui.adapter.CountryListAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class HomeActivity extends BaseActivity implements HomeView {

    @BindView(R.id.countries)
    RecyclerView countriesRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @Inject
    HomePresenter homePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.home_appbar));
        setContentView(R.layout.activity_main);
        homePresenter.setView(this);
    }

    @Override
    protected void inject() {
        ((CountryListApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return homePresenter;
    }

    public void setupCountries(List<Country> countries) {
        countriesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        countriesRecyclerView.setAdapter(new CountryListAdapter(this, countries, homePresenter.getOnElementClickListener()));
        progressBar.setVisibility(View.INVISIBLE);
    }

    /**
     * Display connection error in dialog
     */
    public void showConnectionErrorMessage(DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.connection_error);
        builder.setPositiveButton(R.string.retry, onClickListener);
        builder.create().show();
    }
}
