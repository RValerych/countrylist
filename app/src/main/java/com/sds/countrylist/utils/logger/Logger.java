package com.sds.countrylist.utils.logger;

import android.util.Log;

import com.sds.countrylist.BuildConfig;

/**
 * Custom logger class which uses native android Log and disable it in release build
 */
public class Logger {

    public static void debug(String message) {
        if (BuildConfig.DEBUG) {
            Log.d("Logger", message);
        }
    }

    public static void error(String message) {
        if (BuildConfig.DEBUG) {
            Log.e("Logger", message);
        }
    }

    public static void error(Throwable e) {
        error(e.getMessage());
    }
}
