package com.sds.countrylist.details.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sds.countrylist.CountryListApplication;
import com.sds.countrylist.R;
import com.sds.countrylist.base.activity.BaseActivity;
import com.sds.countrylist.base.presenter.BasePresenter;
import com.sds.countrylist.details.presenter.DetailsPresenter;
import com.sds.countrylist.home.model.Country;
import com.sds.countrylist.utils.svg.SVGUtils;

import javax.inject.Inject;

import butterknife.BindView;

public class DetailsActivity extends BaseActivity implements DetailsView {

    private static final String COUNTRY_KEY = "COUNTRY_KEY";

    @BindView(R.id.country_flag)
    ImageView countryFlag;
    @BindView(R.id.country_name)
    TextView countryName;
    @BindView(R.id.native_name)
    TextView nativeName;
    @BindView(R.id.country_population)
    TextView countryPopulation;
    @BindView(R.id.code)
    TextView code;
    @BindView(R.id.capital)
    TextView capital;
    @BindView(R.id.region)
    TextView region;
    @BindView(R.id.subregion)
    TextView subregion;
    @BindView(R.id.latitude)
    TextView latitude;
    @BindView(R.id.longitude)
    TextView longitude;
    @BindView(R.id.demonym)
    TextView demonym;
    @BindView(R.id.area)
    TextView area;
    @BindView(R.id.timezones)
    TextView timezones;

    @Inject
    DetailsPresenter detailsPresenter;

    public static Intent getIntent(Context context, Country country) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(COUNTRY_KEY, country);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle(getString(R.string.country_details));
        detailsPresenter.setView(this);
        detailsPresenter.setCountry((Country) getIntent().getSerializableExtra(COUNTRY_KEY));
    }

    @Override
    protected void inject() {
        ((CountryListApplication) getApplication()).getApplicationComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return detailsPresenter;
    }

    public void setupCountryDetails(Country country) {
        SVGUtils.insertSVGImage(this, country.getFlag(), countryFlag);
        countryName.setText(country.getName());
        nativeName.setText(getString(R.string.native_name, country.getNativeName()));
        countryPopulation.setText(getString(R.string.population, country.getPopulation()).replace(",", "."));
        code.setText(getString(R.string.code, country.getAlpha2Code()));
        capital.setText(getString(R.string.capital, country.getCapital()));
        region.setText(getString(R.string.region, country.getRegion()));
        subregion.setText(getString(R.string.subregion, country.getSubregion()));
        if (country.getLatlng() != null && country.getLatlng().size() >= 1) {
            latitude.setText(getString(R.string.latitude, country.getLatlng().get(0)));
            longitude.setText(getString(R.string.longitude, country.getLatlng().get(1)));
        } else {
            latitude.setVisibility(View.GONE);
            longitude.setVisibility(View.GONE);
        }
        demonym.setText(getString(R.string.demonym, country.getDemonym()));
        area.setText(getString(R.string.area, Math.round(country.getArea())));
        timezones.setText(getString(R.string.timezones, country.getTimezones().toString().replaceAll("\\[|\\]", "")));
    }
}
