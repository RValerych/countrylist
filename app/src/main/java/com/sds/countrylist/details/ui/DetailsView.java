package com.sds.countrylist.details.ui;

import com.sds.countrylist.home.model.Country;

public interface DetailsView {

    void setupCountryDetails(Country country);
}
