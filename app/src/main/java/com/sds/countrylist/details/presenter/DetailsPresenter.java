package com.sds.countrylist.details.presenter;

import com.sds.countrylist.base.presenter.BasePresenter;
import com.sds.countrylist.details.ui.DetailsView;
import com.sds.countrylist.home.model.Country;

import javax.inject.Inject;

public class DetailsPresenter extends BasePresenter {

    private DetailsView detailsView;

    @Inject
    public DetailsPresenter() {
    }

    public void setView(DetailsView detailsView) {
        this.detailsView = detailsView;
    }

    public void setCountry(Country country) {
        if (country != null) {
            detailsView.setupCountryDetails(country);
        }
    }
}
