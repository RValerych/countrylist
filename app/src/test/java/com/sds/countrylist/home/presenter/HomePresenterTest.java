package com.sds.countrylist.home.presenter;

import com.sds.countrylist.api.RestApiClient;
import com.sds.countrylist.home.model.Country;
import com.sds.countrylist.home.ui.activity.HomeView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class HomePresenterTest {

    @Mock
    RestApiClient restApiClient;
    @Mock
    HomeView homeView;
    @Mock
    Country country;
    @Mock
    Observable<List<Country>> observable;

    private HomePresenter presenter;

    @Before
    public void setup() {
        Mockito.when(restApiClient.getAllCountries()).thenReturn(observable);
        presenter = new HomePresenter(restApiClient);
        presenter.setView(homeView);
    }

    @Test
    public void shouldCallAllCountries() {
        Mockito.verify(restApiClient, times(1)).getAllCountries();
    }

    @Test
    public void shouldStartNewActivityAfterClick() {
        //when
        presenter.getOnElementClickListener().onClick(country);

        //then
        Mockito.verify(homeView, times(1)).startActivity(any());
    }
}
