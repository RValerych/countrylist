package com.sds.countrylist.details.presenter;

import com.sds.countrylist.details.ui.DetailsView;
import com.sds.countrylist.home.model.Country;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class DetailsPresenterTest {

    @Mock
    DetailsView detailsView;
    @Mock
    Country country;

    private DetailsPresenter presenter;

    @Before
    public void setup() {
        presenter = new DetailsPresenter();
        presenter.setView(detailsView);
    }

    @Test
    public void shouldStartNewActivityAfterClick() {
        //when
        presenter.setCountry(country);

        //then
        Mockito.verify(detailsView, times(1)).setupCountryDetails(country);
    }
}
